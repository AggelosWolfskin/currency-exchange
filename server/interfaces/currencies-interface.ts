export interface Currency {
    ISO: string;
    currency: string;
}

export interface ConvertionRate {
    [key:string]:number
}

export interface CurrencyConvertionRate {
    [key:string]:ConvertionRate
}

export interface DB {
    currencies:Currency[],
    rate:CurrencyConvertionRate
}

