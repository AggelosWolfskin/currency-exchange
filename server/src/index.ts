import * as dotenv from "dotenv";
import routes from './routes';
import express from 'express';
import cors from 'cors';

dotenv.config();

const app: express.Application = express();
if (!process.env.PORT) {
   process.exit(1);
}


app.use(cors());

app.use(express.json())

app.use(routes);

const PORT: number = parseInt(process.env.PORT as string, 10);
app.listen(PORT, () => { console.log(`Listening on port ${PORT}...`) });
