import { Router } from 'express';
import currencyRouter from './currency.router';

const routes = Router();

routes.use('/api/currencies', currencyRouter);

export default routes;
