import { ConvertionRate, Currency } from "../../interfaces/currencies-interface";
import express from 'express';
import * as CurrencyService from './currency.service';
import { checkJwt } from "../middleware/authz.middleware";
export const currencyRouter = express.Router();

//Return all currencies
currencyRouter.get('/', async (req, res) => {
    const currencies: Currency[] = await CurrencyService.findAll();
    return res.status(200).send(currencies);
})

//Return specific currency by ISO code
currencyRouter.get("/:ISO", async (req, res) => {
    const currency: Currency | undefined = await CurrencyService.find(req.params.ISO);
    if (currency) {
        return res.status(200).send(currency);
    }
    return res.status(404).send("The currency with the given ISO doesn't exist");
})

//Return all convertion rates of ISO
currencyRouter.get("/rating/:ISO", async (req, res) => {
    try {
        const rate: ConvertionRate = await CurrencyService.findAllRates(req.params.ISO);
        if (!rate) return res.status(404).send("The selected ISO has no convertion rate");
        else return res.status(200).send(rate);
    } catch (error) {
        res.status(500).send(error);
    }
})

//Return Convertion rate of Base to convertTo
currencyRouter.get("/rating/:base/:to", async (req, res) => {
    try {
        const rate: ConvertionRate = await CurrencyService.findRate(req.params.base, req.params.to);
        return rate ? res.status(200).send(rate) : res.status(404).send("The selected pair has no convertion rate");
    } catch (error) {
        res.status(500).send(error);
    }
})

//Protect all below routes
currencyRouter.use(checkJwt);

//Serve update on existing currency
currencyRouter.put("/editor/:ISO", async (req, res) => {
    try {
        const newCurrencies: Currency[] | undefined = await CurrencyService.updateCurrency(req.params.ISO, req.body?.newISO, req.body?.newCurrency);
        return newCurrencies?res.status(200).send(newCurrencies):res.status(404).send("Currency not found");;
    } catch (error) {
        return res.status(400).send(error);
    }
})

//Add new Currency
currencyRouter.post("/editor/", async (req, res) => {
    const { error } = CurrencyService.validateCurrency(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }
    const newCurrency: Currency[] | undefined = await CurrencyService.create(req.body);
    return newCurrency ? res.status(201).send(newCurrency) : res.status(400).send("Currency already exists!");
})

//Serve update on convertion rate of given pair
currencyRouter.put("/editor/rating/update/:ISO", async (req, res) => {
    const newCurrencyRating = await CurrencyService.updateCurrencyRating(req.params.ISO, req.body);
    return newCurrencyRating ? res.status(200).send(newCurrencyRating) : res.status(404).send("Currency not found");
})

//Delete the currency with given ISO
currencyRouter.delete("/editor/:ISO", async (req, res) => {
    try {
        await CurrencyService.remove(req.params.ISO);
        return res.sendStatus(204);
    } catch (error) {
        res.status(404).send(error)
    }
})


export default currencyRouter;
