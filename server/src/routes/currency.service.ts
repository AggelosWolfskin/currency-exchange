import Joi from "joi";
import { DB, Currency, ConvertionRate } from "../../interfaces/currencies-interface";
let dummyDB: DB = {
    currencies: [
        { ISO: "EUR", currency: "Euro" },
        { ISO: "USD", currency: "US Dollar" },
        { ISO: "CHF", currency: "Swiss Franc" },
        { ISO: "GBD", currency: "British Pound" },
        { ISO: "JPY", currency: "Japanese Yen" },
        { ISO: "CAD", currency: "Canadian Dollar" }
    ],
    rate: {
        EUR: { USD: 1.3764, CHF: 1.2079, GBD: 0.8731 },
        USD: { EUR: 0.726532985, CHF: 0.878811846, JPY: 76.72 },
        CHF: { USD: 1.1379, EUR: 0.827883103 },
        GBD: { EUR: 1.145344176, CAD: 1.5648 },
        JPY: { USD: 0.013034411 },
        CAD: { GBD: 0.639059305 }
    }
}

//Find the currency with the given ISO
export const find = async (ISO: string): Promise<Currency | undefined> => dummyDB.currencies.find(c => c.ISO === ISO);

export const findAll = async (): Promise<Currency[]> => dummyDB.currencies;

export const findRate = async (from: string, to: string): Promise<ConvertionRate> => { return { [`${to}`]: dummyDB.rate[from][to] } };

export const findAllRates = async (ISO: string): Promise<ConvertionRate> => dummyDB.rate[ISO];

export const updateCurrency = async (ISO: string, newISO?: string, newCurrency?: string): Promise<Currency[] | undefined> => {
    //Find currency 
    const currency: Currency | undefined = await find(ISO);
    if (currency) {//Form the updated currency
        const updatedCurrency: Currency = {
            ISO: newISO ? newISO : currency.ISO,
            currency: newCurrency ? newCurrency : currency.currency
        }
        //Validate if updated currency has correct format
        const { error } = validateCurrency(updatedCurrency);
        if (newISO && ISO !== newISO) {
            updateRatesIsos(ISO, newISO)
        }
        if (error) {
            throw new Error(error.details[0].message);
        } else {
            //replace old currency in dummy db 
            const index = dummyDB.currencies.indexOf(currency, 0);
            dummyDB.currencies[index] = updatedCurrency;
            //return new currency
            return dummyDB.currencies;
        }
    }
    else return undefined;
}

export const updateCurrencyRating = async (ISO: string, rate: ConvertionRate): Promise<ConvertionRate | undefined> => {
    //Check if base currency exists
    const currency = await find(ISO)
    //Update convertion rates of ISO with new rates
    if (currency) {
        Object.keys(rate).forEach(async key => {
            //Check if ISO key of rate exists as a Currency 
            if (await find(key)) {
                if (!dummyDB.rate[ISO]) dummyDB.rate[ISO] = {};  //Initiate new empty object if curency has no convertion rate pair
                dummyDB.rate[ISO][key] = rate[key];
                //Update the reverse exchange 
                if (!dummyDB.rate[key]) dummyDB.rate[key] = {};// Same logic as line 62
                dummyDB.rate[key][ISO] = 1 / rate[key];
            }
        })
        return dummyDB.rate[ISO];
    } else
        return undefined;
}

export const create = async (currency: Currency): Promise<Currency[] | undefined> => {
    //Check if currency already exists 
    if (!!await find(currency.ISO)) return undefined;
    else
        dummyDB.currencies.push(currency);
    return dummyDB.currencies
}

export const remove = async (ISO: string): Promise<void> => {
    let currency: Currency | undefined = await find(ISO);
    if (currency) {
        const index = dummyDB.currencies.indexOf(currency, 0);
        if (index > -1) {
            dummyDB.currencies.splice(index, 1);
        }
    } else
        throw new Error('Currency not found!')
}

//Validate format of currency
export const validateCurrency = (currency: Currency) => {
    const schema = Joi.object({
        ISO: Joi.string().alphanum().min(3).required(),
        currency: Joi.string().min(3).required()
    });

    return schema.validate(currency);
}



function updateRatesIsos(ISO: string, newISO: string) {
    //Replace ISO name of base currency
    dummyDB.rate[newISO] = dummyDB.rate[ISO]
    delete dummyDB.rate[ISO];
    //For each convertion rate change the old name aswell
    Object.keys(dummyDB.rate).forEach(key => {
        if (key !== newISO) {
            dummyDB.rate[key][newISO] = dummyDB.rate[key][ISO]
            delete dummyDB.rate[key][ISO];
        }
    })
}
