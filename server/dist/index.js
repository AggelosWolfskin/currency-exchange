"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const dummy_db_1 = require("../dummy-db/dummy-db");
const app = (0, express_1.default)();
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => { console.log(`Listening on port ${PORT}...`); });
app.get('/', (req, res) => {
    res.send(dummy_db_1.DB);
});
