"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.currencyRouter = void 0;
const express_1 = __importDefault(require("express"));
const CurrencyService = __importStar(require("./currency.service"));
const authz_middleware_1 = require("../middleware/authz.middleware");
exports.currencyRouter = express_1.default.Router();
//Return all currencies
exports.currencyRouter.get('/', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const currencies = yield CurrencyService.findAll();
    return res.status(200).send(currencies);
}));
//Return specific currency by ISO code
exports.currencyRouter.get("/:ISO", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const currency = yield CurrencyService.find(req.params.ISO);
    if (currency) {
        return res.status(200).send(currency);
    }
    return res.status(404).send("The currency with the given ISO doesn't exist");
}));
//Return all convertion rates of ISO
exports.currencyRouter.get("/rating/:ISO", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const rate = yield CurrencyService.findAllRates(req.params.ISO);
        if (!rate)
            return res.status(404).send("The selected ISO has no convertion rate");
        else
            return res.status(200).send(rate);
    }
    catch (error) {
        res.status(500).send(error);
    }
}));
//Return Convertion rate of Base to convertTo
exports.currencyRouter.get("/rating/:base/:to", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const rate = yield CurrencyService.findRate(req.params.base, req.params.to);
        return rate ? res.status(200).send(rate) : res.status(404).send("The selected pair has no convertion rate");
    }
    catch (error) {
        res.status(500).send(error);
    }
}));
//Protect all below routes
exports.currencyRouter.use(authz_middleware_1.checkJwt);
//Serve update on existing currency
exports.currencyRouter.put("/editor/:ISO", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    try {
        const newCurrencies = yield CurrencyService.updateCurrency(req.params.ISO, (_a = req.body) === null || _a === void 0 ? void 0 : _a.newISO, (_b = req.body) === null || _b === void 0 ? void 0 : _b.newCurrency);
        return newCurrencies ? res.status(200).send(newCurrencies) : res.status(404).send("Currency not found");
        ;
    }
    catch (error) {
        return res.status(400).send(error);
    }
}));
//Add new Currency
exports.currencyRouter.post("/editor/", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { error } = CurrencyService.validateCurrency(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }
    const newCurrency = yield CurrencyService.create(req.body);
    return newCurrency ? res.status(201).send(newCurrency) : res.status(400).send("Currency already exists!");
}));
//Serve update on convertion rate of given pair
exports.currencyRouter.put("/editor/rating/update/:ISO", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const newCurrencyRating = yield CurrencyService.updateCurrencyRating(req.params.ISO, req.body);
    return newCurrencyRating ? res.status(200).send(newCurrencyRating) : res.status(404).send("Currency not found");
}));
//Delete the currency with given ISO
exports.currencyRouter.delete("/editor/:ISO", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield CurrencyService.remove(req.params.ISO);
        return res.sendStatus(204);
    }
    catch (error) {
        res.status(404).send(error);
    }
}));
exports.default = exports.currencyRouter;
