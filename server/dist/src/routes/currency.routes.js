"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dummy_db_1 = require("../../dummy-db/dummy-db");
const joi_1 = __importDefault(require("joi"));
const express_1 = require("express");
const currencyRouter = (0, express_1.Router)();
//Return all currencies
currencyRouter.get('/', (req, res) => {
    return res.status(200).send(dummy_db_1.DB.currencies);
});
//Return specific currency by ISO code
currencyRouter.get("/:ISO", (req, res) => {
    let index = dummy_db_1.DB.currencies.findIndex(curr => { return curr.ISO === req.params.ISO; });
    if (index < 0)
        return res.status(404).send("The currency with the given ISO doesn't exist");
    return res.status(200).send(dummy_db_1.DB.currencies[index]);
});
//Return Convertion rate of Base to convertTo
currencyRouter.get("/:base/:to", (req, res) => {
    const rate = dummy_db_1.DB.rate[req.params.base][req.params.to];
    if (!rate)
        return res.status(404).send("The selected pair has no convertion rate");
    else
        return res.status(200).send({ convertionRate: rate });
});
//Serve update on existing currency
currencyRouter.put("/:ISO", (req, res) => {
    var _a, _b;
    let index = dummy_db_1.DB.currencies.findIndex(curr => { return curr.ISO === req.params.ISO; });
    if (index < 0)
        return res.status(404).send("The currency with the given ISO doesn't exist");
    try {
        const newCurrency = updateCurrency(index, (_a = req.body) === null || _a === void 0 ? void 0 : _a.newISO, (_b = req.body) === null || _b === void 0 ? void 0 : _b.newCurrency);
        return res.status(200).send(newCurrency);
    }
    catch (error) {
        console.log(error);
        return res.status(400).send(error);
    }
});
//Add new Currency
currencyRouter.post("/", (req, res) => {
    const { error } = validateCurrency(req.body);
    if (error) {
        return res.status(400).send(error.details[0].message);
    }
    const newCurrency = { ISO: req.body.ISO, currency: req.body.currency };
    dummy_db_1.DB.currencies.push(newCurrency);
    res.status(200).send(newCurrency);
});
//Validate format of currency
function validateCurrency(currency) {
    const schema = joi_1.default.object({
        ISO: joi_1.default.string().alphanum().min(3).required(),
        currency: joi_1.default.string().min(3).required()
    });
    return schema.validate(currency);
}
//Currency
function updateCurrency(index, newISO, newCurrency) {
    const oldCurrency = dummy_db_1.DB.currencies[index];
    //Get the optional parameters
    const updatedCurrency = {
        ISO: newISO ? newISO : oldCurrency.ISO,
        currency: newCurrency ? newCurrency : oldCurrency.currency
    };
    //Validate if updated currency has correct format
    const { error } = validateCurrency(updatedCurrency);
    if (error) {
        throw new Error(error.details[0].message);
    }
    else {
        //replace old currency in dummy db 
        dummy_db_1.DB.currencies[index] = updatedCurrency;
        //return new currency
        return updatedCurrency;
    }
}
exports.default = currencyRouter;
