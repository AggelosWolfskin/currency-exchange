"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateCurrency = exports.remove = exports.create = exports.updateCurrencyRating = exports.updateCurrency = exports.findAllRates = exports.findRate = exports.findAll = exports.find = void 0;
const joi_1 = __importDefault(require("joi"));
let dummyDB = {
    currencies: [
        { ISO: "EUR", currency: "Euro" },
        { ISO: "USD", currency: "US Dollar" },
        { ISO: "CHF", currency: "Swiss Franc" },
        { ISO: "GBD", currency: "British Pound" },
        { ISO: "JPY", currency: "Japanese Yen" },
        { ISO: "CAD", currency: "Canadian Dollar" }
    ],
    rate: {
        EUR: { USD: 1.3764, CHF: 1.2079, GBD: 0.8731 },
        USD: { EUR: 0.726532985, CHF: 0.878811846, JPY: 76.72 },
        CHF: { USD: 1.1379, EUR: 0.827883103 },
        GBD: { EUR: 1.145344176, CAD: 1.5648 },
        JPY: { USD: 0.013034411 },
        CAD: { GBD: 0.639059305 }
    }
};
//Find the currency with the given ISO
const find = (ISO) => __awaiter(void 0, void 0, void 0, function* () { return dummyDB.currencies.find(c => c.ISO === ISO); });
exports.find = find;
const findAll = () => __awaiter(void 0, void 0, void 0, function* () { return dummyDB.currencies; });
exports.findAll = findAll;
const findRate = (from, to) => __awaiter(void 0, void 0, void 0, function* () { return { [`${to}`]: dummyDB.rate[from][to] }; });
exports.findRate = findRate;
const findAllRates = (ISO) => __awaiter(void 0, void 0, void 0, function* () { return dummyDB.rate[ISO]; });
exports.findAllRates = findAllRates;
const updateCurrency = (ISO, newISO, newCurrency) => __awaiter(void 0, void 0, void 0, function* () {
    //Find currency 
    const currency = yield (0, exports.find)(ISO);
    if (currency) { //Form the updated currency
        const updatedCurrency = {
            ISO: newISO ? newISO : currency.ISO,
            currency: newCurrency ? newCurrency : currency.currency
        };
        //Validate if updated currency has correct format
        const { error } = (0, exports.validateCurrency)(updatedCurrency);
        if (newISO && ISO !== newISO) {
            updateRatesIsos(ISO, newISO);
        }
        if (error) {
            throw new Error(error.details[0].message);
        }
        else {
            //replace old currency in dummy db 
            const index = dummyDB.currencies.indexOf(currency, 0);
            dummyDB.currencies[index] = updatedCurrency;
            //return new currency
            return dummyDB.currencies;
        }
    }
    else
        return undefined;
});
exports.updateCurrency = updateCurrency;
const updateCurrencyRating = (ISO, rate) => __awaiter(void 0, void 0, void 0, function* () {
    //Check if base currency exists
    const currency = yield (0, exports.find)(ISO);
    //Update convertion rates of ISO with new rates
    if (currency) {
        Object.keys(rate).forEach((key) => __awaiter(void 0, void 0, void 0, function* () {
            //Check if ISO key of rate exists as a Currency 
            if (yield (0, exports.find)(key)) {
                if (!dummyDB.rate[ISO])
                    dummyDB.rate[ISO] = {}; //Initiate new empty object if curency has no convertion rate pair
                dummyDB.rate[ISO][key] = rate[key];
                //Update the reverse exchange 
                if (!dummyDB.rate[key])
                    dummyDB.rate[key] = {}; // Same logic as line 62
                dummyDB.rate[key][ISO] = 1 / rate[key];
            }
        }));
        return dummyDB.rate[ISO];
    }
    else
        return undefined;
});
exports.updateCurrencyRating = updateCurrencyRating;
const create = (currency) => __awaiter(void 0, void 0, void 0, function* () {
    //Check if currency already exists 
    if (!!(yield (0, exports.find)(currency.ISO)))
        return undefined;
    else
        dummyDB.currencies.push(currency);
    return dummyDB.currencies;
});
exports.create = create;
const remove = (ISO) => __awaiter(void 0, void 0, void 0, function* () {
    let currency = yield (0, exports.find)(ISO);
    if (currency) {
        const index = dummyDB.currencies.indexOf(currency, 0);
        if (index > -1) {
            dummyDB.currencies.splice(index, 1);
        }
    }
    else
        throw new Error('Currency not found!');
});
exports.remove = remove;
//Validate format of currency
const validateCurrency = (currency) => {
    const schema = joi_1.default.object({
        ISO: joi_1.default.string().alphanum().min(3).required(),
        currency: joi_1.default.string().min(3).required()
    });
    return schema.validate(currency);
};
exports.validateCurrency = validateCurrency;
function updateRatesIsos(ISO, newISO) {
    //Replace ISO name of base currency
    dummyDB.rate[newISO] = dummyDB.rate[ISO];
    delete dummyDB.rate[ISO];
    //For each convertion rate change the old name aswell
    Object.keys(dummyDB.rate).forEach(key => {
        if (key !== newISO) {
            dummyDB.rate[key][newISO] = dummyDB.rate[key][ISO];
            delete dummyDB.rate[key][ISO];
        }
    });
}
