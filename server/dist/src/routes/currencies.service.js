"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAll = exports.find = void 0;
var dummyDB = {
    currencies: [
        { ISO: "EUR", currency: "Euro" },
        { ISO: "USD", currency: "Dollar" },
        { ISO: "CHF", currency: "Swiss franc" }
    ],
    rate: {
        "EUR": { "USD": 1.2, "CHF": 0.4 },
        "USD": { "EUR": 0.83, "CHF": 0.7 },
        "CHF": { "EUR": 2.5, "USD": 1.42857142857 }
    }
};
const find = (ISO) => __awaiter(void 0, void 0, void 0, function* () { return dummyDB.currencies.filter(curr => ISO === curr.ISO); });
exports.find = find;
const findAll = () => __awaiter(void 0, void 0, void 0, function* () { return dummyDB.currencies; });
exports.findAll = findAll;
