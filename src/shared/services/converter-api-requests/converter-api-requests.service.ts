import { Injectable } from '@angular/core';
import { ConvertionRate, Currency } from '../../interfaces/currency.interface';
import { environment as env } from '../../../environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ConverterApiRequestsService {

  constructor(private httpClient: HttpClient) { }

  //Add new currency
  public addCurrency(newCurrency:Currency): Observable<Currency[]> {
    const headers = new HttpHeaders({ 'Accept': 'application/json' })
    return this.httpClient.post<Currency[]>(`${env.dev.serverUrl}/api/currencies/editor/`, newCurrency, { headers })
  }

  //get specific currency
  public getCurrency(ISO: string): Observable<Currency> {
    return this.httpClient.get<Currency>(`${env.dev.serverUrl}/api/currencies/${ISO}`);
  }

  //Get list of all currencies 
  public getCurrencies(): Observable<Currency[]> {
    return this.httpClient.get<Currency[]>(`${env.dev.serverUrl}/api/currencies`);
  }

  //update selected currency
  public updateCurrency(ISO: string, newISO?: string, newCurrency?: string):Observable<Currency[]> {
    console.log(ISO, newISO, newCurrency)
    const body: { [k: string]: string } = {};
    if (newISO) body['newISO'] = newISO;
    if (newCurrency) body['newCurrency'] = newCurrency;
    const headers = new HttpHeaders({ 'Accept': 'application/json' })
    return this.httpClient.put<Currency[]>(`${env.dev.serverUrl}/api/currencies/editor/${ISO}`, body, { headers })
  }

  //Delete selected currency
  public deleteCurrency(ISO:string) {
    return this.httpClient.get<ConvertionRate>(`${env.dev.serverUrl}/api/currencies/editor/${ISO}`);
  }

  //Get convertion rate of specific To currency
  public getCovertionRates(from: string, to: string): Observable<ConvertionRate> {
    return this.httpClient.get<ConvertionRate>(`${env.dev.serverUrl}/api/currencies/rating/${from}/${to}`);
  }

  //TODO: Update selected currency's convertion rate
  public updateConvertionRate(ISO:string,data:ConvertionRate) {
    const headers = new HttpHeaders({ 'Accept': 'application/json' })
    return this.httpClient.put<ConvertionRate>(`${env.dev.serverUrl}/api/currencies/editor/rating/update/${ISO}`, data, { headers })
  }

  //Delete selected currency's convertion rate
  public deleteConvertionRate(ISO:string) {

  }
}
