import { TestBed } from '@angular/core/testing';

import { ConverterApiRequestsService } from './converter-api-requests.service';

describe('ConverterApiRequestsService', () => {
  let service: ConverterApiRequestsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConverterApiRequestsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
