export interface Currency {
    ISO: string;
    currency: string;
    symbol?: symbol;
}

export interface ConvertionRate {
    [key:string]:number
}

export interface CurrencyConvertionRate {
    [key:string]:ConvertionRate
}


