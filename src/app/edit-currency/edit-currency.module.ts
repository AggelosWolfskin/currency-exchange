import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditCurrencyComponent } from './edit-currency.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { RouterModule } from '@angular/router';
import { AddCurrencyComponent } from './add-currency/add-currency.component';
import { EditConvertionRateComponent } from './edit-convertion-rate/edit-convertion-rate.component';



@NgModule({
  declarations: [
    EditCurrencyComponent,
    AddCurrencyComponent,
    EditConvertionRateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSelectModule,
    MatInputModule,
    MatButtonModule,
    RouterModule
  ]
})
export class EditCurrencyModule { }
