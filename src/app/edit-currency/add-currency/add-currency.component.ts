import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConverterApiRequestsService } from '@services/converter-api-requests/converter-api-requests.service';
import { Currency } from 'src/shared/interfaces/currency.interface';

@Component({
  selector: 'app-add-currency',
  templateUrl: './add-currency.component.html',
  styleUrls: ['./add-currency.component.scss']
})
export class AddCurrencyComponent implements OnInit {
  @Output() newCurrency = new EventEmitter<{}>();
  newCurrGroup: FormGroup;
  constructor(private cars: ConverterApiRequestsService, private fb: FormBuilder) {
    this.newCurrGroup = fb.group({
      ISO: ["", [Validators.required, Validators.minLength(3)]],
      currency: ["", [Validators.required, Validators.minLength(3)]]
    })
  }

  ngOnInit(): void {
  }

  submitNew() {
    if (this.newCurrGroup.valid)
      this.cars.addCurrency(this.newCurrGroup.value as Currency)
      .toPromise()
      .then(newList =>{
        const data = {
          newCurrencyList : newList,
          newSelectedBase: this.newCurrGroup.value as Currency
        }
        this.newCurrency.emit(data);
      },(error)=>{
        console.log(error.status)
        this.newCurrGroup.controls['ISO'].setErrors({'dublicate':true})
      })
  }

  getErrorMessage(){
    return this.newCurrGroup.controls['ISO'].hasError('dublicate')? 'Currency already exists' : "";
  }
}
