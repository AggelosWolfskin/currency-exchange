import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConverterApiRequestsService } from '@services/converter-api-requests/converter-api-requests.service';
import { Subscription } from 'rxjs';
import { Currency } from 'src/shared/interfaces/currency.interface';

@Component({
  selector: 'app-edit-currency',
  templateUrl: './edit-currency.component.html',
  styleUrls: ['./edit-currency.component.scss']
})
export class EditCurrencyComponent implements OnInit, OnDestroy {
  private sub: Subscription = new Subscription();

  editCurrency: boolean = true;
  baseCurrGroup:FormGroup;
  editFormGroup: FormGroup;
  btnText: string = "Add New";
  titleText: string = "Edit Currency";
  currencyList: Currency[] = [];
  selectedCurrency: Currency | undefined;


  constructor(private cars: ConverterApiRequestsService, private fb: FormBuilder) {

    this.baseCurrGroup = fb.group({currencySelect:["", Validators.required]});
    this.editFormGroup = fb.group({
      newISO: ["", Validators.minLength(3)],
      newCurrency: ["", Validators.minLength(3)]
    }
    )
  }

  ngOnInit(): void {
    //Get all the currencies and populate select field options
    this.sub.add(this.cars.getCurrencies().subscribe(currencies => {
      this.currencyList = currencies;
    }))

    //Listen select currency and fetch convertion rates of selected currency;
    this.listenCurrencySelection();

  }



  private listenCurrencySelection() {
    this.sub.add(this.baseCurrGroup.controls['currencySelect'].valueChanges.subscribe(value => {
      this.selectedCurrency = value;
      //Populate form fields with current ISO and Currency 
      this.editFormGroup.patchValue({
        'newISO': this.selectedCurrency?.ISO,
        'newCurrency': this.selectedCurrency?.currency
      });

    }));
  }

  //Submit edits of currencies
  submitEdits(): void {
    if (this.selectedCurrency && this.editFormGroup.valid){
    const newISO = this.editFormGroup.controls['newISO'].value
    const newCurrency = this.editFormGroup.controls['newCurrency'].value
      this.cars.updateCurrency(this.selectedCurrency.ISO, newISO, newCurrency)
      .toPromise().then(result=>{
        //update currency list with new edits
        this.currencyList = result;
        //update edited currency on front
        const newCurrency = this.currencyList.find(c => c.ISO === newISO);
        this.selectedCurrency = newCurrency;
        this.baseCurrGroup.controls['currencySelect'].patchValue(this.selectedCurrency)
      })}
  }

  changeForm() {
    this.editCurrency = !this.editCurrency;
    this.btnText = this.editCurrency ? "Add New" : "Edit Currency";
    this.titleText = this.editCurrency ? "Edit Currency" : "Add New Currency";
  }

  updateCurrencyList(data: any) {
    this.changeForm();//return to editing
    this.currencyList = data.newCurrencyList;
    this.selectedCurrency = data.newSelectedBase
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }
}
