import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ConverterApiRequestsService } from '@services/converter-api-requests/converter-api-requests.service';
import { Subscription } from 'rxjs';
import { ConvertionRate, Currency } from 'src/shared/interfaces/currency.interface';

@Component({
  selector: 'app-edit-convertion-rate',
  templateUrl: './edit-convertion-rate.component.html',
  styleUrls: ['./edit-convertion-rate.component.scss']
})
export class EditConvertionRateComponent implements OnInit {
  //List of all currencies
  @Input() currencyList?: Currency[];
  //Base Currency
  private _baseCurrency:Currency | undefined
  @Input() set baseCurrency(currency:Currency | undefined){
    this._baseCurrency=currency;
    this.fetchConvertionPairs()
  };
  get baseCurrency(): Currency | undefined{
    return this._baseCurrency;
  }
  //Input to update or create currency
  private sub: Subscription = new Subscription();
  convertionForm: FormGroup;
  selConvCurr: Currency | undefined;
  convRate: ConvertionRate | undefined;;
  constructor(private cars: ConverterApiRequestsService, private fb: FormBuilder) {
    this.convertionForm = fb.group({
      convertionCurrency: [""],
      convertionRate: [0, Validators.min(0)]
    })
  }

  ngOnInit(): void {

    //Listen changes on convert-to currency
    this.listenConvertionSelection();
  }

  private listenConvertionSelection() {
    this.sub.add(this.convertionForm.get('convertionCurrency')?.valueChanges.subscribe(_cr => {
      this.selConvCurr = _cr;
      
      this.fetchConvertionPairs();
    }
    ));
  }

  private fetchConvertionPairs() {
    if (this.baseCurrency && this.selConvCurr) {
      //If its the same ISO rate is 1 
      if (this.baseCurrency.ISO === this.selConvCurr.ISO) {
        this.convertionForm.controls['convertionRate'].patchValue(1);
        return;
      }
      //If not same ISO get convertion rate 
      this.cars.getCovertionRates(this.baseCurrency.ISO, this.selConvCurr.ISO).toPromise().then(cr => {
        this.convRate = cr;
        //FIll convertion rate field with current value
        if (this.selConvCurr)
          this.convertionForm.get('convertionRate')?.patchValue(this.convRate[this.selConvCurr.ISO]);
      }, reject => {
        this.convertionForm.get('convertionRate')?.patchValue(0);
      });
    }
  }

  submitEdits(){
    if(this.convertionForm.valid && this.baseCurrency){
        const ISO:string =  this.convertionForm?.get("convertionCurrency")?.value.ISO;
        const rate:number =  this.convertionForm?.get("convertionRate")?.value;
        const data: ConvertionRate = {
          [`${ISO}`]:rate
        }
        this.cars.updateConvertionRate(this.baseCurrency.ISO,data).toPromise().then();
    }
  }

  ngOnDestroy() {
    //close all subscriptions when component destroyed
    this.sub.unsubscribe();
  }
}
