import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditConvertionRateComponent } from './edit-convertion-rate.component';

describe('EditConvertionRateComponent', () => {
  let component: EditConvertionRateComponent;
  let fixture: ComponentFixture<EditConvertionRateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditConvertionRateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditConvertionRateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
