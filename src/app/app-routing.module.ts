import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@auth0/auth0-angular';
import { CalculatorComponent } from './calculator/calculator.component';
import { EditCurrencyComponent } from './edit-currency/edit-currency.component';
const routes: Routes = [
  {
    path: "",
    component: CalculatorComponent,
    loadChildren: () => import('./calculator/calculator.module').then(m => m.CalculatorModule)
  },
  {
    path: "edit",
    component: EditCurrencyComponent,
    loadChildren: () => import('./edit-currency/edit-currency.module').then(m => m.EditCurrencyModule),
    //Require login from user to access edit features
    canActivate: [AuthGuard]
  }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
