import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { logoPath } from 'src/settings/configuration';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  logoPath = logoPath;
  constructor() { }

  ngOnInit(): void {
  }

}
