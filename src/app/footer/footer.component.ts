import { Component, OnInit } from '@angular/core';
import { contactInfo, logoPath } from 'src/settings/configuration';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
  logoPath = logoPath;
  contactInfo = contactInfo;
  constructor() { }

  ngOnInit(): void {
  }

}
