import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { ConverterApiRequestsService } from '@services/converter-api-requests/converter-api-requests.service';
import { Currency } from 'src/shared/interfaces/currency.interface';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.scss']
})
export class CalculatorComponent implements OnInit {


  groupForm: FormGroup;

  currencyList: Currency[] = [];
  selectedBase: Currency | undefined;
  selectedConvertion: Currency | undefined;
  convertionRate: number = 0;
  convertionResult: number = 0;
  selectedAmount: number = 1;

  constructor(private cars: ConverterApiRequestsService,private fb: FormBuilder) { 
    this.groupForm = fb.group({
      baseCurr: ["", Validators.required],
      convertTo: ["", Validators.required],
      amount: [1,[Validators.required, Validators.min(0)]]
    })
  }

  ngOnInit(): void {
    //Get list of all currencies 
    this.cars.getCurrencies().subscribe(currencies => {
      console.log("ta carrencies", currencies);
      this.currencyList = currencies;
    });

    this.groupForm.get("baseCurr")?.valueChanges.subscribe(base => {
      if (base) {
        this.selectedBase = base;
        this.fetchConvertionRate();
        this.calculateConvertion();
      }
    })

    this.groupForm.get("convertTo")?.valueChanges.subscribe(to => {
      if (to) {
        this.selectedConvertion = to;
        this.fetchConvertionRate();
        this.calculateConvertion();
      }
    })

    this.groupForm.get("amount")?.valueChanges.subscribe(amount => {
      if (amount) {
        this.selectedAmount = amount;
        this.calculateConvertion();
      }
    })

  }

  private fetchConvertionRate() {
    if (this.selectedBase && this.selectedConvertion)
      this.cars.getCovertionRates(this.selectedBase.ISO, this.selectedConvertion.ISO).toPromise().then(result => {
        this.convertionRate = this.selectedConvertion ? result[this.selectedConvertion.ISO] : 0;
      });
  }

  //Calculate converted currency with applied convertion rate on base currency
  calculateConvertion() {
    this.convertionResult = this.groupForm.get("amount")?.value * this.convertionRate ?? 0;
  }

  getErrorMessage() {
    if (this.groupForm.controls['baseCurr'].hasError('required')) return 'You must enter a value';
    if (this.groupForm.controls['convertTo'].hasError('required')) return 'You must enter a value';
    if (this.groupForm.controls['amount'].hasError('min')) return 'You must enter a higher number';
    return undefined
  }
}

