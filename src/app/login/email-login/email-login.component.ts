import { Component, OnInit } from '@angular/core';
import { AuthService } from '@auth0/auth0-angular';

@Component({
  selector: 'app-email-login',
  templateUrl: './email-login.component.html',
  styleUrls: ['./email-login.component.scss']
})
export class EmailLoginComponent implements OnInit {
  hide=true;
  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

  //Redirect to auth0 login form
  //If successfull auth0 will redirect back to home page
  loginWithRedirect():void {
    this.auth.loginWithRedirect()
  }

  //Redirect to auth0 signup form
  //If successfull auth0 will redirect back to home page
  signUpWithRedirect(): void {
    this.auth.loginWithRedirect({ screen_hint: 'signup' });
  }

  //Logout user
  //Clears and refreshes session
  logout(): void {
    this.auth.logout();
  }
}
