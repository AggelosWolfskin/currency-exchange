# Currency Exchange Calculator

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.0.3.

## Development Front-End server

Run these commands in the terminal to run the app on your local environment

`git clone git@gitlab.com:AggelosWolfskin/currency-exchange.git`

or `git clone https://gitlab.com/AggelosWolfskin/currency-exchange.git` if you have no ssh keys set up on GitLab

`cd currency-exchange`

`npm install` to install required npm modules

`ng serve` a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Development Backend Node.js 

`cd ./server` assuming you are already under currency-exchange directory

Run `npm install` to install required npm modules

Run `npm run dev` a dev backend server.




